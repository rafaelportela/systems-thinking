# Holistic Thinking

A holistic approach must include all three notions of structure, function, and
process.

In a classical concept of reality, a specific structure S causes a particular
function F and different structures cause different functions. It's assumed
that to understand a system, we need to know only its structure.

```
S1 -----> F1
S2 -----> F2
```

However, when several outcomes are produced in the same environment by a given
structure, then knowledge of the process becomes as necessary to understand the
whole as the knowledge of environment, structure, and function.

Structure, function, and process, along woth the environment or context, form
an interdependent set of mutually exclusive and collectively exhaustive
variables. Together, these four perspectives define the whole or make the
understanding of the whole possible.

## 5.2 Systems dimensions

