# ch4 Development

Development is a core concept of the systems view of the world. I contrst to
the mechanistic and biological views concerned, respectively, with efficiency
and growth, the systems view is basically concerned with development.

```
mechanistic -> efficiency
biological -> growth
system -> development
```

Presents a table grouping other development theories by how singular or plural
their assumptions of function, structure, and process are.

## 4.2 Systems view of development

Development of an organization is a purposeful transformation towards higher
levels of integration and differentiation.

Differentiation represents an artistic orientation (looking for differences
among things that are apparently similar) emphasizing stylistc values and
signifying tendencies towards increased complexity, variety, autonomy, and
morphogenesis (creation of a new structure).

Integration, on the other hand, represents a scientific orientation (looking
for similarities among things that are apparently different) emphasizing
instrumental values and signifying tendencies torward increased order,
uniformity, conformity, collectivity, and morphostasis (maintenance of
structure).

```
                order       organized       organized
                            simplicity      complexity
integration
                chaos       chaotic         chaotic
                            simplicity      complexity

 
                             simple          complex

                                 diferentiation
```
      

[figure 4.2 levels of differentiation and integration]

Depending on the caracteristics of a given culture, a social system can move
from a state of chaotic simplicity torward organized simplicity, which is
produced by emphasizing integration at the cost of differenntiation.

```
integration

  ^   organized simplicity
 / \
  |
  |
  |
  |   chaotic simplicity
```

It can also move toward chaotic complexity produced by increased
differentiation at the cost of integration ...

```
differentiation

 ------------------>

 chaotic      chaotic
 simplicity   complexity
```

... or it can move toward organized
complexity, signifying a higher level of organization achieved by a movement
toward complexy and order concurrentily.

```
development

      /\    organized
      /     complexity
     /
    /  chaotic
   /   simplicity
```

This measns that for every level of differentiation there exists a minimun
level of integration below which the system would disintegrate into chaos.
Conversely, higher levels of integration require higher degrees of
differentiation to avoid impotency.

Development of socual systems is a transformation into successive modes of
organization. Each mode is a whole, characterized by higher degrees of both
integration and differentiation, and is potentially capable of disoolving lower
level contradictions by converting them into contraries. In contrast to
physical systems whose energy level determines their mode of organization, in
socual systems the knowledge level defines the mode. One does not lose
knowledge by sharing it with others. On the ocntrary, its dissemination
increases the knowledge level of the social system and helps the creation of
new knowledge. It is this capability that enables a social system of its own
accord to constantly re-create its structure and redefine its functions.

*desire and ability*

In defining development, we identify two active agents: desire and ability.

Desire is produced by an exciting vision of a future enhanced by the
interaction of creative and recreative (joyful) processes. The creative
capacity of man, along with his/her desire to share, results in a shared image
of a desired future. This generates dissatisfaction with the present and
motivates pursuit of more challenging and more desirable ends.

Dissatisfaction with the present, although a necessary condition for change, is
not sufficient to ensure development. What seems to be necessary as well is
a faith in one's ability to partly control the march of events.

Ability, therefore, is the potential for controlling, influencing, and
appreciating the parameters that affect the system's existence. But ability
alone cannt ensure development. Without a shared image of a more desirable
future, the frustration of the powerful masses can easily be converted into
a unifying agent of change - hatred - that in turn will successfully destroy
the present but will not necessarily be a step toward creating a better future.

*helping, or limiting, desire and ability*

To understand the process of development of a social system we have to deal
with structures and the processes that help or limit the creation of collective
desire and ability for the pursuit of its ends.

A minimun level of integration is required if the aggregate of individuals is
to function as an effective system. Ultimately, the level of integration and
development that an organization will achieve depends on the means by which it
deals with interaction among its members.

Differentiation poses little challenge because it is the very nature of social
systems to become different from each other. Integration, however, requires
skill to accomplish. To integrate one has to appreciate the systemic nature of
the interactions between opposing tendencies.

![Developmental processes](../images/ch4-devproc.png)

integration vs differentiation -> development
security vs freedom -> participation
stability vs change -> adaptation
collectively vs individually -> socialization
uniformity vs uniqueness -> innovation
order vs complexity -> organization


## 4.3 obstruction to development 

Obstruction to development of a social system can be viewed as malfunctioning
in any one of the five dimensions.

Primary obstructions: Scarcity, maldistribution, and insecurity in any one of
the five social functions (generation and dissemination of knowledge, power,
wealth, vaues and beauty).

Secondary obstructions: Alienation, polarization, corruption and terrorism.

### 4.3.5 Recap

* Development of an organization is a purposeful transformation toward higher
  levels of integration and differentiatin. Is is a collective learning process
by which a social system increases its _ability_ and _desire_ to serve itself,
its members, and its environment.

* For every level of differentiation there exists a minimun level of
  integration below which the system woudl disintegrate into chaos. Conversely,
higher levels of integration require higher degrees of differentiation to avoid
sterility.

* Unless an organization effectively server the purpose of its containing
  systems and its purposeful parts, they will not serve it well. This requires
that the organization be designed to enable the parts to operate as independent
systems with the ability yo be relatively self-controlling while acting as
responsible parts of a coherent whole that has the right to make collective
choices.

